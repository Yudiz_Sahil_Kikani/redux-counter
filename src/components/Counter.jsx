import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'

class Counter extends Component {
  increment = () => {
    // console.log('increment')
    this.props.dispatch({ type: 'INCREMENT' })
  }

  decrement = () => {
    // console.log('decrement')
    this.props.dispatch({ type: 'DECREMENT' })
  }

  render () {
    return (
        <div className='counter'>
            <h1>Redux counter</h1>
            <div>
                <p>Counter: {this.props.count}</p>
                <button onClick={this.increment}>
                    Increment
                </button>
                <button onClick={this.decrement}>
                    Decrement
                </button>
            </div>
        </div>
    )
  }
}

function mapStateToProps (state) {
  console.log(state)
  return {
    count: state.count
  }
}
Counter.propTypes = {
  dispatch: PropTypes.func,
  count: PropTypes.number
}
export default connect(mapStateToProps)(Counter)
